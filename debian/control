Source: python-apptools
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Varun Hiremath <varun@debian.org>, Stewart Ferguson <stew@ferg.aero>
Build-Depends:
 debhelper (>= 12),
 debhelper-compat (= 12),
 dh-python,
 python3-all,
 python3-setuptools,
 python3-traitsui,
 python3-tables,
 python3-pandas,
 python3-nose,
 python3-configobj,
 python3-sphinx,
 xauth,
 xvfb,
Standards-Version: 4.4.1
Homepage: https://pypi.python.org/pypi/apptools
Vcs-Git: https://salsa.debian.org/python-team/packages/python-apptools.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-apptools

Package: python3-apptools
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Recommends: python3-traitsui
Suggests: python-apptools-doc
Description: ETS Application Tools (Python 3)
 The AppTools project includes a set of packages that Enthought has
 found useful in creating a number of applications. They implement
 functionality that is commonly needed by many applications.
 .
 This is the Python 3 release of the package.

Package: python-apptools-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Breaks: python-apptools (<< 4.4.0)
Replaces: python-apptools (<< 4.4.0)
Description: ETS Application Tools (doc)
 The AppTools project includes a set of packages that Enthought has
 found useful in creating a number of applications. They implement
 functionality that is commonly needed by many applications.
 .
 This is the documentation release of the package including manpages,
 examples, and HTML documentation.
